(function($){
$(document).ready(function () {


  // Menu Mobile Toggle
  $('.navbar-toggle').click(function() {
    //console.log("clicked");
    $('body').toggleClass("nav-expanded");
  });


  $('.sub-menu-parent .menu-toggle').click(function(e) {
    e.preventDefault();
    if($(this).parent().hasClass('toggled')){
      $('.opened').removeClass("opened");
      $('.toggled').removeClass('toggled');
      $(this).parent().removeClass("toggled");
    } else{
      $('.opened').removeClass("opened");
      $('.toggled').removeClass('toggled');
      $('.sub-menu-parent.toggled .sub-menu').addClass("opened");
      $(this).parent().addClass("toggled");
    }
    $('.sub-menu-parent.toggled .sub-menu').addClass("opened");

    //return false;
  });


  });

})(jQuery);
