  (function($) {
$(document).ready(function () {

  $('#utaCarousel').find('.item').first().addClass('active');

  $('.uta-angebot-akkordeon').find('.panel-collapse').first().addClass('in');
  $('.uta-angebot-akkordeon').find('a').first().addClass('akkordeon-out');
  $('.uta-angebot-akkordeon').find('a').first().find('.fa').css( { 'transition': 'all 0.01s linear','transform': 'rotate(90deg)'});



  $('.akkordeon-item.item_1').find('.panel-collapse').addClass('in');
  $('.akkordeon-item.item_1').find('a').addClass('akkordeon-out');
  $('.akkordeon-item.item_1').find('a').find('.fa').css( { 'transition': 'all 0.01s linear','transform': 'rotate(90deg)'});

checkCollapsed();

});

function checkCollapsed() {
        $('.collapse').on('shown.bs.collapse', function (e) {
            triggerCollapsedClass();
        }).on('hidden.bs.collapse', function (e) {
            triggerCollapsedClass();
        });
    }

    function triggerCollapsedClass() {
        $("[data-toggle=collapse]").each(function (i, item) {
            var selector = $(item).attr("href");
            if ($(selector).hasClass("in")) {
                $(item).addClass("akkordeon-out");
                $(item).find('.fa').css( { 'transition': 'all 0.2s linear','transform': 'rotate(90deg)'});
            } else {
                $(item).removeClass("akkordeon-out");
                $(item).find('.fa').css( {'transition': 'all 0.2s linear','transform': 'rotate(0deg)'});
            }
        })
    }

})(jQuery);

(function($) {
  $(window).on("load", function () {
    // scrolls to the block "Aktuelle Angebote" on the startpage in the mobile version
    // Additional note: this code is put inside $(window).on("load") function because there are problems
    // with Safari mobile browser if this code is executed inside the $(document).ready() function:
    // In Safari the offset().top gives a wrong value, see here https://gist.github.com/mataspetrikas/431639
    // The solution is to use this function inside $(window).on("load"): then all images are loaded and 
    // probably other stuff and the Safari can calculate the correct offset to the block element.
    var elem = $('#block-angebotnext');
    // the width 768 is taken as a criteria to recognize the mobile version
    if ($(window).width() < 768) {
      // the shift of 75px is because of the height of the menu header, which appears in the mobile version  
      $(window).scrollTop(elem.offset().top - 75);
    }
  });

})(jQuery);
