<?php

/**
 * @file
 * Konzert buchen Modul
 */

 use Drupal\Component\Render\FormattableMarkup;
 use \Drupal\node\Entity\Node;
 use Drupal\node\NodeInterface;

 function konzert_buchen_page_attachments(array &$page) {
   $page['#attached']['library'][] = 'konzert_buchen/konzert_buchen';
}


 function konzert_buchen_mail($key, &$message, $params) {
   if ($message['module'] == 'konzert_buchen') {

     $angebot_id = $params['angebot_id'];
     $node = \Drupal::entityTypeManager()->getStorage('node')->load($angebot_id);

     $angebot_titel = $node->getTitle();

     $angebot_zeit_anfang = format_date(strtotime($node->get('field_zeitraum')->value), "uta_date_short", "", "Europe/Berlin");
     $angebot_uhrzeit = '';
     $angebot_uhrzeit = $node->get('field_uhrzeiten_info')->value;

     $key = $message['key'];

     $zahlungsweise_labels = [
       '0' => 'Abbuchung Gesamtbetrag',
     ];

     // $zahlungsweise_labels = [
     //   '0' => 'Abbuchung Gesamtbetrag',
     //   '1' => 'Überweisung Gesamtbetrag',
     // ];

     $geschlecht_labels = [
       'frau' => 'Frau',
       'mann' => 'Herr',
     ];
     $anrede_user = $params['geschlecht'] == 'frau' ? 'Liebe' : 'Lieber';

     $anzahl_erwachsene = floatval($params['erwachsene']) + 1;
     $anzahl_kinder = floatval($params['kinder']);
     preg_match('/\d+/', $params['preis_tarif'], $matches);
     $gesamtbetrag = floatval($matches[0]) * $anzahl_erwachsene + floatval($matches[0]) * $anzahl_kinder / 2;

     $arguments = [
       ':geschlecht' => $geschlecht_labels[$params['geschlecht']],
       ':anrede_user' => $anrede_user,
       ':vorname' => $params['vorname'],
       ':nachname' => $params['nachname'],
       ':firma' => $params['firma'],
       ':plz' => $params['plz'],
       ':ort' => $params['ort'],
       ':land' => $params['land'],
       ':strasse' => $params['strasse'],
       ':telefon' => $params['telefon'],
       ':uebernachtung' => $params['uebernachtung'],
       ':email' => $params['email'],
       '@sonstige_mitteilungen' => $params['sonstige_mitteilungen'],
       ':zahlungsweise' => $zahlungsweise_labels[$params['zahlungsweise_konzert']],
       ':erwachsene' => $anzahl_erwachsene,
       ':kinder' => $anzahl_kinder,
       ':gesamtbetrag' => number_format($gesamtbetrag, 2, ',', ''),
       ':preis_tarif' => $params['preis_tarif'],
       ':kontoinhaber' => $params['kontoinhaber_konzert'],
       ':iban' => $params['iban_konzert'],
       ':bic' => $params['bic_konzert'],
       ':name_der_bank' => $params['name_der_bank_konzert'],
       '@angebot_titel' => $angebot_titel,
       ':angebot_zeit_anfang' => $angebot_zeit_anfang,
       ':angebot_uhrzeit' => $angebot_uhrzeit,
       ':mandatsnr' => $params['mandatsnr'],
      ];

     $markup_zahlungsdaten_client = '';
     $markup_zahlungsdaten_uta = '';

     if($params['zahlungsweise_konzert'] < 1) {
       $markup_zahlungsdaten_uta = '
KontoinhaberIn:                        :kontoinhaber
IBAN:                                  :iban
BIC:                                   :bic
Bank:                                  :name_der_bank

Du hast dich für die Zahlung per SEPA-Lastschrift entschieden. Hiermit möchten wir dich über den Einzug von :gesamtbetrag € zum Fälligkeitstag '.date("d.m.Y").' informieren. Folgende Referenzen zu deiner Information: Gläubiger-Identifikationsnr.: DE76ZZZ00001009535 Mandatsnummer: :mandatsnr.
';
       // we want to hide out iban in the email to the client
       // covered with XXX symbols, i.e. DEXXXXXXXXXXXXX397
       $iban_client = str_replace(' ', '', $params['iban_konzert']);
       if (strlen($iban_client) > 5) {
         $iban_client = substr($iban_client, 0, 2) . str_repeat('X', strlen($iban_client)-5) . substr($iban_client, -3, 3);
       }
       $markup_zahlungsdaten_client = '
KontoinhaberIn:                              :kontoinhaber
IBAN:                                        '.$iban_client.'

Du hast dich für die Zahlung per SEPA-Lastschrift entschieden. '.$markup_einzug.' Folgende Referenzen zu deiner Information: 
Gläubiger-Identifikationsnr.: DE76ZZZ00001009535 Mandatsnummer: :mandatsnr
';
     } else {
       $markup_zahlungsdaten_client = '
       :zahlungsweise
       
___Unsere_Bankverbindung___
UTA Cologne GmbH, Commerzbank Köln
IBAN: DE91 3704 0044 01294 909 00
BIC: COBADEFFXXX
Bitte gib bei Überweisungen den Titel und das Datum der gebuchten Veranstaltung an.
';
     }
      $message['headers']['From'] = 'Osho UTA Institut <buchung@oshouta.info>';

     switch ($key) {

   	  case 'send_mail_client':

       $message['subject'] = t('Buchungsbestätigung: @seminar', array('@seminar' => $angebot_titel));




         $text = ':anrede_user :vorname :nachname,

vielen Dank für deine verbindliche Buchung.
 
 
Du hast die folgende Veranstaltung gebucht:
 
@angebot_titel
Termin: :angebot_zeit_anfang
:angebot_uhrzeit
Preistarif: :preis_tarif
Anzahl Erwachsene: :erwachsene
Anzahl Kinder: :kinder
Gesamtbetrag: :gesamtbetrag €
 
 
Deine Angaben wurden wie folgt erfolgreich an uns übermittelt:
 
 
___Deine Kontaktdaten___
Vorname:                                    :vorname
Nachname:                                   :nachname
Firma:                                      :firma
Straße, Hausnummer:                         :strasse
Plz:                                        :plz
Ort:                                        :ort
Land:                                       :land
Telefon:                                    :telefon
Email:                                      :email
 
 
___Deine_Zahlungsweise___
'.$markup_zahlungsdaten_client.'
 
 
___Deine_Nachricht___ (optional)
@sonstige_mitteilungen
 
 
Informationen zur Anreise und Wissenswertes zum Osho UTA Institut und der Umgebung findest du auf unserer Webseite.

Bei Fragen stehen wir dir gerne zur Verfügung: unter + 49 221 57407-0 oder events@oshouta.de

Wir freuen uns auf dich.

Herzliche Grüße

Das Veranstaltungsteam des Oshos UTA Instituts
';


         $text_formatted = new FormattableMarkup($text, $arguments);
         $message['body'][] = $text_formatted->__toString();


    break;

    case 'send_mail_uta':
    $message['subject'] = t('Neue Anmeldung von @vorname @nachname', array('@vorname' => $params['vorname'], '@nachname' => $params['nachname']));

      //$token = \Drupal::token();

      $text = 'Anmeldung von: :vorname :nachname (:email)

Folgende Daten wurden übermittelt:

Angebot:
@angebot_titel
:angebot_zeit_anfang
:angebot_uhrzeit
Preistarif: :preis_tarif
Anzahl Erwachsene: :erwachsene
Anzahl Kinder: :kinder
Gesamtbetrag: :gesamtbetrag €
 
 
___Kontaktdaten___
Anrede:                                   :geschlecht
Vorname:                                  :vorname
Nachname:                                 :nachname
Firma:                                    :firma
Straße, Hausnummer:                       :strasse
Plz:                                      :plz
Ort:                                      :ort
Land:                                     :land
Telefon:                                  :telefon
 
 
___Zahlung___
Gesamtbetrag:                      :gesamtbetrag €
Zahlungsweise:
:zahlungsweise
'. $markup_zahlungsdaten_uta .'
 
___Nachricht___
@sonstige_mitteilungen
';


      $text_formatted = new FormattableMarkup($text, $arguments);
      $message['body'][] = $text_formatted->__toString();


    break;
    }
   }
 }

?>
