<?php

/**
 * @file
 * Contains \Drupal\uta_standort\Plugin\Block\UtaStandortBlock.
 */

namespace Drupal\uta_standort\Plugin\Block;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatch;



/**
 * Provides the UTA Standort Block.
 *
 * @Block(
 *   id="uta_standort_block",
 *   admin_label = @Translation("UTA Standort"),
 * )
 */
class UtaStandortBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

	$build = array();
	$build['#cache']['max-age'] = 0;

	$build['#attached']['library'][] = 'uta_standort/uta_standort';


	$language = \Drupal::languageManager()->getCurrentLanguage();
	$current_langcode = $language->getId();

	$module_path = drupal_get_path('module', 'uta_standort');

	$build['#theme'] = 'uta_standort_map';
	$build['#geo_lat'] = 50.9410190;
  $build['#geo_lng'] = 6.9374620;
  $build['#uta_adress'] = "Venloerstr. 5-7, 50672 Köln";

  $build['#uta_headline'] = 'Osho UTA Institut';

  if(strstr($_SERVER['HTTP_HOST'],'preview-server'))
  	{
      //Preview Server / Domain
      $build['#uta_maps_api'] = 'AIzaSyBDL5XY8D1wJxLccSgbPdyVt1GxoVcsA2Q';
    }
    else {
      //Live Server / Domain
      $build['#uta_maps_api'] = 'AIzaSyD3me6g1aPa8GJlgFVey_t4H_a5m64Lp34';
    }


    return $build;
  }
}
