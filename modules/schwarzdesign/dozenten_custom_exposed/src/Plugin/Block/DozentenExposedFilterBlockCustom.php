<?php

namespace Drupal\dozenten_custom_exposed\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;

/**
 * Provides a 'Einzelsitzungen Exposed Filter Custom' Block.
 *
 * @Block(
 *   id = "dozenten_custom_exposed",
 *   admin_label = @Translation("Dozenten Exposed Filter Block"),
 * )
 */
class DozentenExposedFilterBlockCustom extends BlockBase {

  /**
   * {@inheritdoc}
   */

  public function build() {

    $build = array();
    $build['#cache']['max-age'] = 0;

    $build['#attached']['library'][] = 'dozenten_custom_exposed/dozenten_filter';

    $checkbox = "";


if(isset($_GET['einzelsitzungen']) && ($_GET['einzelsitzungen'] == 1)){
  $checkbox = '
  <input type="checkbox" class="form-checkbox" id="edit-einzelsitzungen" name="einzelsitzungen" value="1" checked="checked">
  <label for="edit-einzelsitzungen" class="control-label option">Einzelsitzungen
  </label>';
} else {
  $checkbox = '
  <input type="checkbox" class="form-checkbox" id="edit-einzelsitzungen" name="einzelsitzungen" value="1">
  <label for="edit-einzelsitzungen" class="control-label option">Einzelsitzungen
  </label>';
}


// Taxonomy Vokabular für Themen
$tax_themen = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('angebotsthema', $parent = 0, $max_depth = TRUE, $load_entities = FALSE);
$thema_selected = ' selected';
$select_markup_themen = '';
foreach($tax_themen as $term){
  //print_r($term->name);
  //print_r($term->tid);
  $select_markup_themen .= '<option value="'.$term->tid.'"';
  if(isset($_GET['themenbereich']) && !empty($_GET['themenbereich'])){
    if($term->tid == $_GET['themenbereich']){
      $select_markup_themen .= ' selected';
      $thema_selected = '';
    }
  }
  $select_markup_themen .= '>'.$term->name.'</option>';
}


    $build['#markup'] = t('
    <div id="custom_filter_dozenten" class="container main-content">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <form class="views-exposed-form bef-exposed-form" data-bef-auto-submit-full-form="" data-drupal-selector="views-exposed-form-dozenten-index-block-1" action="" method="get" id="views-exposed-form-dozenten-index-block-1" accept-charset="UTF-8">
      <div class="einzelsitzungen">
      '.$checkbox.'
      </div>
      <div class="select-wrapper themenbereiche">
      <select class="form-control form-select" id="edit-themenbereich--3" name="themenbereich">
      <option value="All" '.$thema_selected.'>Alle Themen</option>
        '.$select_markup_themen.'
      </select>
      </div>
      </form>
    </div>
    </div>
    </div>
      ');


    return $build;


  }

}
