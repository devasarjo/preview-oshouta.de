<?php

/**
 * @file
 * Contains \Drupal\terminliste_angebot\Plugin\Block\TerminlisteAngebotBlock.
 */

namespace Drupal\terminliste_angebot\Plugin\Block;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatch;


/**
 * Provides a 'Terminliste Angebot Block' block.
 *
 * @Block(
 *   id = "terminliste_angebot_block",
 *   admin_label = @Translation("TerminlisteAngebotBlock"),
 *   category = @Translation("Blocks")
 * )
 */
class TerminlisteAngebotBlock extends BlockBase {



  public function build() {

	$build = array();
	// Attach library.
	$build['#cache']['max-age'] = 0;


	$terminliste_angebot_content = "";

    $terminliste_angebot_content .= $this->generateTerminlisteAngebot();


	$build['content'] = array(
      '#markup' => $terminliste_angebot_content
    );



    return $build;
  }

  /**
   * Display the Main nav items
   *
   * @return string  an html link to the next or previous node
   */
  private function generateTerminlisteAngebot() {

//echo "<br />generateTerminlisteAngebot";
	$counter = 0;
  $terminliste_content = '';
  $terminliste_content_item = '';

	$language = \Drupal::languageManager()->getCurrentLanguage();
	$current_langcode = $language->getId();
	//echo "<br />current_langcode: " . $current_langcode;


	$current_path = \Drupal::service('path.current')->getPath();
	//echo "<br />current_path: " . $current_path;

	if ($node = \Drupal::routeMatch()->getParameter('node')) {
	$current_nid = $node->nid->value;
	$current_title = $node->title->value;
	$current_pagetype = $node->getType();

	}
	else
	{
		$current_nid = "";
		$current_title = "";
		$current_pagetype = "";
	}

if($current_pagetype == "angebot") {
  // echo "<br />current_pagetype: " . $current_pagetype;
  // echo "<br />current_nid: " . $current_nid;

$result = db_query("SELECT value.node_id as 'Node_ID', value.date_start as 'Start_Date', value.date_end as 'End_Date', value.nur_anfang,
value.doz, value.doz_name, value.pid
FROM
(
(
SELECT n.nid as node_id,
fdu.field_datum_uhrzeit_value as date_start,
fdu.field_datum_uhrzeit_end_value as date_end,
az.field_nur_anfangszeit_anzeigen_value as nur_anfang,
dz.field_dozenten_target_id as doz,
dz_name.field_name_value as doz_name,
p.profile_id as pid
FROM node__field_datum_uhrzeit as fdu
LEFT JOIN node__field_termin as ft ON (ft.field_termin_target_id = fdu.entity_id)
LEFT JOIN node_field_data as n ON (n.nid = ft.entity_id)
LEFT JOIN node__field_nur_anfangszeit_anzeigen as az
ON(az.entity_id = ft.field_termin_target_id AND az.deleted = 0)
LEFT JOIN node__field_dozenten as dz
ON(dz.entity_id = ft.field_termin_target_id AND dz.deleted = 0)
LEFT JOIN user__field_name as dz_name
ON(dz.field_dozenten_target_id = dz_name.entity_id AND dz_name.deleted = 0)
LEFT JOIN profile as p
ON(dz.field_dozenten_target_id = p.uid AND p.status = 1 and p.type = 'dozent')
)
UNION
(
SELECT n.nid as node_id,
ftr.field_termin_regel_value as date_start,
ftr.field_termin_regel_end_value as date_end,
NULL as nur_anfang,
NULL as doz,
NULL as doz_name,
NULL as pid
FROM date_recur__node__field_termin_regel as ftr
LEFT JOIN node_field_data as n ON (n.nid = ftr.entity_id)
INNER JOIN node__field_termin_regel as tr ON (tr.entity_id = ftr.entity_id AND tr.deleted = 0)
 )
) value
WHERE value.node_id = '".$current_nid."'
ORDER BY value.date_start;");

// $result->allowRowCount = TRUE;
// $counter_rows = $result->rowCount();
// echo "<br />counter_rows: " . $counter_rows;

while($row = $result->fetchAssoc()){


  $tmp_start = $row['Start_Date'];
  $tmp_ende = $row['End_Date'];
  $tmp_nur_anfang = $row['nur_anfang'];
  if($tmp_nur_anfang == "") {
    $tmp_nur_anfang = 0;
  }

  $tmp_dozent = $row['doz'];
  if($tmp_dozent != "") {
    $tmp_dozent_string = ', mit <a href="/de/profile/'.$row['pid'].'" title="'.$row['doz_name'].'" target="_self">' . $row['doz_name'] . '</a>';
  }
  else {
    $tmp_dozent_string = '';
  }


date_default_timezone_set('UTC');
$tmp_start_timestamp = strtotime($tmp_start);
$tmp_ende_timestamp = strtotime($tmp_ende);

$tmp_date_formatted = $this->formatterTerminString($tmp_start_timestamp,$tmp_ende_timestamp,$tmp_nur_anfang);

$terminliste_content_item .= '<li class="terminliste_item">' .$tmp_date_formatted. $tmp_dozent_string. '</li>';

$counter ++;
}

}

if($counter > 1){
  $terminliste_content .= '<h3>Termine</h3>
  <div class="termin_teaser row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<ul class="terminliste">'.$terminliste_content_item.'</ul>
</div>
</div>';
}

	return $terminliste_content;
  }



/**
 * Formatter for Termin Dates
 *
 * @return string  DateString
 */

 private function formatterTerminString($utc_termin_start_timestamp,$utc_termin_ende_timestamp,$nur_anfang) {


  date_default_timezone_set('Europe/Berlin');
  $date_formatted = '';

  $start_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_short');
  $ende_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_short');

  if($start_formatted_short != $ende_formatted_short) {
    $date_formatted = $start_formatted_short . ' - ' . $ende_formatted_short;
  }
  else {

    if($nur_anfang == 1) {
      $start_formatted_time = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_time');
      $date_formatted = $start_formatted_short . ', ab ' . $start_formatted_time . ' Uhr';
    }
    else {
      $start_formatted = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_medium_date');
      $ende_formatted_time = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_time');
      $date_formatted = $start_formatted . ' - ' . $ende_formatted_time . ' Uhr';
    }
  }


  return $date_formatted;
}



}
