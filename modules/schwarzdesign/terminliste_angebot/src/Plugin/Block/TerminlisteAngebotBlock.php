<?php

/**
 * @file
 * Contains \Drupal\terminliste_angebot\Plugin\Block\TerminlisteAngebotBlock.
 */

namespace Drupal\terminliste_angebot\Plugin\Block;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatch;


/**
 * Provides a 'Terminliste Angebot Block' block.
 *
 * @Block(
 *   id = "terminliste_angebot_block",
 *   admin_label = @Translation("TerminlisteAngebotBlock"),
 *   category = @Translation("Blocks")
 * )
 */
class TerminlisteAngebotBlock extends BlockBase {



  public function build() {

	$build = array();
	// Attach library.
	$build['#cache']['max-age'] = 0;


	$terminliste_angebot_content = "";

    $terminliste_angebot_content .= $this->generateTerminlisteAngebot();


	$build['content'] = array(
      '#markup' => $terminliste_angebot_content
    );



    return $build;
  }

  /**
   * Display the Main nav items
   *
   * @return string  an html link to the next or previous node
   */
  private function generateTerminlisteAngebot() {

//echo "<br />generateTerminlisteAngebot";
	$counter = 0;
  $terminliste_content = '';
  $terminliste_content_item = '';
  $today = date('Y-m-d');

	$language = \Drupal::languageManager()->getCurrentLanguage();
	$current_langcode = $language->getId();
	//echo "<br />current_langcode: " . $current_langcode;


	$current_path = \Drupal::service('path.current')->getPath();
	//echo "<br />current_path: " . $current_path;

	if ($node = \Drupal::routeMatch()->getParameter('node')) {
	$current_nid = $node->nid->value;
	$current_title = $node->title->value;
	$current_pagetype = $node->getType();

	}
	else
	{
		$current_nid = "";
		$current_title = "";
		$current_pagetype = "";
	}

if($current_pagetype == "angebot") {
  // echo "<br />current_pagetype: " . $current_pagetype;
  // echo "<br />current_nid: " . $current_nid;
  // echo "<br />today: " . $today;

$result = db_query("SELECT value.node_id as 'Node_ID', value.termin_id, value.date_start as 'Start_Date', value.date_end as 'End_Date', value.nur_anfang, value.keine_uhrzeit
FROM
(
(
SELECT n.nid as node_id,
ft.field_termin_target_id as termin_id,
fdu.field_datum_uhrzeit_value as date_start,
fdu.field_datum_uhrzeit_end_value as date_end,
az.field_nur_anfangszeit_anzeigen_value as nur_anfang,
ku.field_keine_uhrzeit_anzeigen_value as keine_uhrzeit
FROM node__field_datum_uhrzeit as fdu
LEFT JOIN node__field_termin as ft ON (ft.field_termin_target_id = fdu.entity_id AND ft.langcode = '".$current_langcode."')
LEFT JOIN node_field_data as n ON (n.nid = ft.entity_id AND n.langcode = '".$current_langcode."')
LEFT JOIN node__field_nur_anfangszeit_anzeigen as az
ON(az.entity_id = ft.field_termin_target_id AND az.deleted = 0)
LEFT JOIN node__field_keine_uhrzeit_anzeigen as ku
ON(ku.entity_id = ft.field_termin_target_id AND ku.deleted = 0)
)
) value
WHERE value.node_id = '".$current_nid."'
AND value.date_start >= '".$today."'
ORDER BY value.date_start;");


// $result->allowRowCount = TRUE;
// $counter_rows = $result->rowCount();
// echo "<br />counter_rows: " . $counter_rows;



while($row = $result->fetchAssoc()){

  $doz_total = 0;
  $tmp_dozent_string = '';
  $tmp_start = $row['Start_Date'];
  $tmp_ende = $row['End_Date'];
  $tmp_nur_anfang = $row['nur_anfang'];
  if($tmp_nur_anfang == "") {
    $tmp_nur_anfang = 0;
  }
  $tmp_keine_uhrzeit = $row['keine_uhrzeit'];
  if($tmp_keine_uhrzeit == "") {
    $tmp_keine_uhrzeit = 0;
  }

  $entity_manager = \Drupal::entityTypeManager();
  if($row['termin_id'] != 0) {
    $termin_node = $entity_manager->getStorage('node')->load($row['termin_id']);
    $termin_title = $termin_node->get('title')->value;
    $termin_dozenten = $termin_node->get('field_dozenten');

    // echo "<br />Termin Title: " . $termin_title;
    $doz_total = count($termin_dozenten);

  if($doz_total > 0) {
    for ($i = 0; $i < $doz_total; $i++) {
      $doz_user_id = $termin_dozenten[$i]->target_id;
      // echo "<br />doz_user_id: ". $doz_user_id;
      $doz_profile_info = $this->getProfileFomUser($doz_user_id);
      $doz_profile_id = $doz_profile_info['profile_id'];
      // echo "<br />doz_profile_id: ". $doz_profile_id;
      $doz_profile_name = $doz_profile_info['profile_name'];
      // echo "<br />doz_profile_name: ". $doz_profile_name;
      if($i == 0) {
        $tmp_dozent_string = ', mit <a href="/de/profile/'.$doz_profile_id.'" title="'.$doz_profile_name.'" target="_self">' . $doz_profile_name. '</a>';
      } else {
        $tmp_dozent_string .= ', <a href="/de/profile/'.$doz_profile_id.'" title="'.$doz_profile_name.'" target="_self">' . $doz_profile_name. '</a>';
      }
    }
  }

}



date_default_timezone_set('UTC');
$tmp_start_timestamp = strtotime($tmp_start);
$tmp_ende_timestamp = strtotime($tmp_ende);

$tmp_date_formatted = $this->formatterTerminString($tmp_start_timestamp,$tmp_ende_timestamp,$tmp_nur_anfang,$tmp_keine_uhrzeit);

$terminliste_content_item .= '<li class="terminliste_item">' .$tmp_date_formatted. $tmp_dozent_string. '</li>';

$counter ++;
}

}



if($counter >= 1){
  $terminliste_content .= '<h3>Termine</h3>
  <div class="termin_teaser row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<ul class="terminliste">'.$terminliste_content_item.'</ul>
</div>
</div>';
}

	return $terminliste_content;
  }



/**
 * Formatter for Termin Dates
 *
 * @return string  DateString
 */

 private function formatterTerminString($utc_termin_start_timestamp,$utc_termin_ende_timestamp,$nur_anfang,$keine_uhrzeit) {


  date_default_timezone_set('Europe/Berlin');
  $date_formatted = '';

  $start_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_short');
  $ende_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_short');

  if($start_formatted_short != $ende_formatted_short) {
    $date_formatted = $start_formatted_short . ' - ' . $ende_formatted_short;
  }
  else {

    if($nur_anfang == 1) {
      $start_formatted_time = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_time');

      if($keine_uhrzeit == 1) {
        $date_formatted = $start_formatted_short;
      } else {
        $date_formatted = $start_formatted_short . ', ab ' . $start_formatted_time . ' Uhr';
      }
    }
    else {
      $start_formatted = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_medium_date_days');
      $ende_formatted_time = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_time');
      if($keine_uhrzeit == 1) {
        $date_formatted = $start_formatted_short . ' - ' . $ende_formatted_short;
      }
      else {
        $date_formatted = $start_formatted . ' - ' . $ende_formatted_time . ' Uhr';
      }
    }

    
  }


  return $date_formatted;
}


/**
 * Profile Info
 *
 * @return ArrayObject  ProfileObject
 */
private function getProfileFomUser($user_uid) {

// echo "<br />user id: " . $user_uid;

$profile_info = array();

$profile_info['profile_id'] = "";


$result = db_query("SELECT p.profile_id,
n.field_name_value
FROM profile AS p
LEFT JOIN user__field_name as n
ON (n.entity_id = p.uid and n.deleted = 0)
WHERE p.uid = ".$user_uid."
AND p.type = 'dozent'
AND p.status = 1");

if($result)
{

 while($row = $result->fetchAssoc()){

    $profile_info['profile_id'] = $row['profile_id'];
    $profile_info['profile_name'] = $row['field_name_value'];

  }

}

  return $profile_info;
}


}
