<?php

/**
 * @file
 * Contains \Drupal\angebot_uebersicht\Plugin\Block\AngbotUebersichtBlock.
 */

namespace Drupal\angebot_uebersicht\Plugin\Block;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Component\Gettext\PoItem;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Routing\RouteMatch;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\node\Entity\Node;

/**
 * Provides the WeitereKursangeboteBlock.
 *
 * @Block(
 *   id="angebot_uebersicht",
 *   admin_label = @Translation("Angebot Uebersicht"),
 * )
 */
class AngebotUebersichtBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

	$build = array();

	$build['#attached']['library'][] = 'angebot_uebersicht/angebot_uebersicht';
  $day_today = date('Y-m-d');

    $datum = ( isset ($_GET["datum"]) AND ($_GET["datum"] != "")) ? $_GET["datum"] : $day_today;
    //echo "<br />datum: " . $datum;
    $thema = ( isset ($_GET["thema"])) ? $_GET["thema"] : 'All';
    $typ = ( isset ($_GET["typ"])) ? $_GET["typ"] : 'All';
    $dozent = ( isset ($_GET["dozent"])) ? $_GET["dozent"] : '';
    $page = ( isset ($_GET["page"])) ? $_GET["page"] : 0;

    // $build['#test'] = date('Y-m-d h:i:s');

    $angebot_content = $this->generateAngebote($datum, $thema, $typ, $dozent, $page);
    $build['#data'] = $angebot_content;
    $pages_total = $angebot_content[0]['pages'];
    $page_active = $angebot_content[0]['active_page'];
    $page_active_title = $page_active + 1;
    $page_before = $page_active - 1;
    $page_before_title = $page_before + 1;
    $page_before2 = $page_active - 2;
    $page_before2_title = $page_before2 + 1;
    $page_after = $page_active + 1;
    $page_after_title = $page_after + 1;
    $page_after2 = $page_active + 2;
    $page_after2_title = $page_after2 + 1;
    $page_previous = $page_active - 1;
    $page_next = $page_active + 1;
    $pages_last = $pages_total;

    // echo "<br />pages_total: " . $pages_total;
    // echo "<br />page_active: " . $page_active;

    $path_without_page = '?typ='.$typ.'&amp;thema='.$thema.'&amp;dozent='.$dozent.'&amp;datum='.$datum;



    $active_page_item = '<li class="pager__item is-active active">
          <a href="'.$path_without_page.'&amp;page='.$page_active.'" title="Aktuelle Seite" rel="prev">
            <span aria-hidden="true">'.$page_active_title.'</span>
          </a>
        </li>';

    $before_active_page_item = '<li class="pager__item">
        <a href="'.$path_without_page.'&amp;page='.$page_before.'" title="'.$page_before_title.'. Seite" rel="prev">
          <span aria-hidden="true">'.$page_before_title.'</span>
        </a>
      </li>';

    $before2_active_page_item = '<li class="pager__item">
          <a href="'.$path_without_page.'&amp;page='.$page_before2.'" title="'.$page_before2_title.'. Seite" rel="prev">
            <span aria-hidden="true">'.$page_before2_title.'</span>
          </a>
        </li>';

    $after_active_page_item = '<li class="pager__item">
          <a href="'.$path_without_page.'&amp;page='.$page_after.'" title="'.$page_after_title.'. Seite" rel="prev">
            <span aria-hidden="true">'.$page_after_title.'</span>
          </a>
          </li>';

    $after2_active_page_item = '<li class="pager__item">
          <a href="'.$path_without_page.'&amp;page='.$page_after2.'" title="'.$page_after2_title.'. Seite" rel="prev">
            <span aria-hidden="true">'.$page_after2_title.'</span>
          </a>
          </li>';

    $previous_page_item = '<li class="pager__item pager__item--previous">
        <a href="'.$path_without_page.'&amp;page='.$page_previous.'" title="Vorherige Seite" rel="prev">
          <span aria-hidden="true">‹ Vorherige</span>
        </a>
      </li>';

    $next_page_item = '<li class="pager__item pager__item--next">
          <a href="'.$path_without_page.'&amp;page='.$page_next.'" title="Nächste Seite" rel="prev">
            <span aria-hidden="true"><span aria-hidden="true">Nächste ›</span></span>
          </a>
        </li>';

    $first_page_item = '<li class="pager__item pager__item--first">
          <a href="'.$path_without_page.'&amp;page=0" title="Zur ersten Seite" rel="prev">
            <span aria-hidden="true">« Erste</span>
          </a>
        </li>';

    $last_page_item = '<li class="pager__item pager__item--last">
        <a href="'.$path_without_page.'&amp;page='.$pages_last.'" title="Zur letzten Seite" rel="last">
          <span aria-hidden="true">Letzte »</span>
        </a>
      </li>';


      // echo "<br />pages_total: " . $pages_total;
      // echo "<br />page_active: " . $page_active;
      //
      // echo "<br />page_before: " . $page_before;
      // echo "<br />page_before2: " . $page_before2;


      if($page_before < 0) {
        $before_active_page_item = "";
      }

      if($page_before2 < 0) {
        $before2_active_page_item = "";
      }

      if($page_after > $pages_total) {
        $after_active_page_item = "";
      }

      if($page_after2 > $pages_total) {
        $after2_active_page_item = "";
      }

    if($pages_total < 3)  {
      $first_page_item = "";
      $previous_page_item = "";
      $next_page_item = "";
      $last_page_item = "";
    }
    elseif($page_active == 0) {
      $first_page_item = "";
      $previous_page_item = "";
      $before_active_page_item = "";
      $before2_active_page_item = "";
    }
    elseif($page_active == 1) {
      $before2_active_page_item = "";
    }
    elseif($page_active == ($pages_total - 1)) {
      $after2_active_page_item = "";
    }
    elseif($page_active == $pages_total) {
      $next_page_item = "";
      $last_page_item = "";
      $after_active_page_item = "";
      $after2_active_page_item = "";
    }



if($pages_total == 0)  {
  $pagination = '';
}
else {
  $pagination = '<nav class="pager-nav text-center" role="navigation" aria-labelledby="pagination-heading"><ul class="pagination">';
  $pagination .= $first_page_item.$previous_page_item.$before2_active_page_item.$before_active_page_item.$active_page_item.$after_active_page_item.$after2_active_page_item.$next_page_item.$last_page_item;
  $pagination .= '</ul></nav>';
}

    // $pagination = '';
    $build['#pager'] = $pagination;

		// $build['#row_counter_flag'] = 3;

		$build['#theme'] = 'angebot_uebersicht';
    $build['#cache']['max-age'] = 0;

    return $build;
  }

  /**
 * Success Story teaser
 *
 * @return array
 */
private function generateAngebote($a_datum, $a_thema, $a_typ, $a_dozent, $a_page) {

// Hier ggf umdefinieren: Tägliche Meditationen, Kurzmassagen, Einzelsitzungen
$daily_med_type = 18;
$kurzmassagenkalender_type = 45;

$test_date = new DrupalDateTime();
$result = array();
$my_results = array();
$counter = 0;
//echo "<br />a_datum: " . $a_datum;

$items_per_page = 10;
$limit_start = $items_per_page * $a_page;
$limit_string = 'LIMIT '.$limit_start.', ' . $items_per_page;
// $limit_string = '';
// echo "<br />limit_string - " . $limit_string;

$language = \Drupal::languageManager()->getCurrentLanguage();

$current_langcode = $language->getId();

$thema_string = "AND thema.field_thema_target_id = ".$a_thema;


if(($a_typ != "") AND ($a_typ != "All")) {
  $typ_string = "AND typ.field_typ_target_id = ".$a_typ;
} else {
  $typ_string = "AND typ.field_typ_target_id NOT IN (18,45)";
}


if($a_dozent != "") {
  $dozent_join = "LEFT JOIN node__field_dozenten as doz ON (doz.entity_id = value.node_id)";
  $dozent_join .= "LEFT JOIN user__field_name as dozname ON (doz.field_dozenten_target_id = dozname.entity_id)";
  $dozent_string = "AND dozname.field_name_value LIKE '%".$a_dozent."%'";
}
else {
  $dozent_join = "";
  $dozent_string = "";
}


$result = db_query("SELECT SQL_CALC_FOUND_ROWS DISTINCT value.node_id as 'Node_ID', value.date_start as 'Start_Date', value.date_end as 'End_Date', value.nur_anfang, value.termintyp, value.keine_uhrzeit

FROM
(
  (SELECT n.nid as node_id, fdu.field_datum_uhrzeit_value as date_start, fdu.field_datum_uhrzeit_end_value as date_end, az.field_nur_anfangszeit_anzeigen_value as nur_anfang, 0 as termintyp, ku.field_keine_uhrzeit_anzeigen_value as keine_uhrzeit
  FROM node__field_datum_uhrzeit as fdu
  INNER JOIN node__field_termin as ft
  ON (ft.field_termin_target_id = fdu.entity_id)
  INNER JOIN node_field_data as n
  ON (n.nid = ft.entity_id AND n.status = 1)
  LEFT JOIN node__field_nur_anfangszeit_anzeigen as az
  ON(az.entity_id = ft.field_termin_target_id AND az.deleted = 0)
  LEFT JOIN node__field_keine_uhrzeit_anzeigen as ku
  ON(ku.entity_id = ft.field_termin_target_id AND ku.deleted = 0)
  )
  UNION
(
  SELECT n.nid as node_id, ftr.field_termin_regel_value_raw as date_start, ftr.field_termin_regel_end_value_raw as date_end, NULL as nur_anfang, 1 as termintyp, NULL as keine_uhrzeit
  FROM date_recur__node__field_termin_regel as ftr
  INNER JOIN node_field_data as n
  ON (n.nid = ftr.entity_id AND n.status = 1)
  INNER JOIN node__field_termin_regel as tr
  ON (tr.entity_id = ftr.entity_id AND tr.deleted = 0)
)
) value


LEFT JOIN node__field_thema as thema
ON (thema.entity_id = value.node_id)
LEFT JOIN node__field_typ as typ
ON (typ.entity_id = value.node_id)
" . $dozent_join. "

WHERE 1
".( $a_thema == 'All' || $a_thema == '' ? '' : $thema_string )."
".$typ_string. "
".$dozent_string. "
AND value.date_start >= '".$a_datum."'
ORDER BY value.date_start
".$limit_string.";
");


$result2 = db_query("SELECT FOUND_ROWS() AS total_results;");
$result2->allowRowCount = TRUE;
$counter_rows = $result2->rowCount();
$row2 = $result2->fetchAssoc();
$total_results = $row2['total_results'];
// echo "<br />total_results: " . $total_results;
$pages = $total_results/$items_per_page;

$my_results[0]['pages'] = floor($pages);
$my_results[0]['active_page'] = $a_page;

//print_r($result->fetchAssoc());
while($row = $result->fetchAssoc()){


  $entity_manager = \Drupal::entityTypeManager();
  $node = $entity_manager->getStorage('node')->load($row['Node_ID']);



  $tmp_title = $node->get('title')->value;
  $tmp_thema = $node->get('field_thema')->target_id;
  if($tmp_thema == "")
  {
    $tmp_thema = 0;
  }
  $tmp_typ = $node->get('field_typ')->target_id;
  $tmp_path_alias = \Drupal::service('path.alias_manager')->getAliasByPath('/node/'.$row['Node_ID']);

  $dozenten = $node->get('field_dozenten');
  $counter_dozent = 0;
  $tmp_doz_string = '';
  $tmp_doz_array = array();
  foreach ($dozenten as $dozenten_ids) {

    $dozenten_uid = get_profile_dozent_from_user($dozenten_ids->target_id);
    $dozenten_name = get_name_from_user($dozenten_ids->target_id);
    $tmp_doz_array[$counter] = '<a href="/profile/'.$dozenten_uid['profile_id'].'">'.$dozenten_name.'</a>';

    if($counter_dozent == 0) {
      $tmp_doz_string .= $tmp_doz_array[$counter];
    } else {
      $tmp_doz_string .= ','.$tmp_doz_array[$counter];
    }

    $counter_dozent ++;

  }

  $tmp_start = $row['Start_Date'];
  $tmp_ende = $row['End_Date'];
  $tmp_nur_anfang = $row['nur_anfang'];
  $tmp_keine_uhrzeit = $row['keine_uhrzeit'];
  if($tmp_nur_anfang == "") {
    $tmp_nur_anfang = 0;
  }
// echo "<br />tmp_start: " . $tmp_start;
// echo "<br />tmp_ende: " . $tmp_ende;
// echo "<br />termintyp: " .$row['termintyp'];

date_default_timezone_set('UTC');
$tmp_start_timestamp = strtotime($tmp_start);
$tmp_ende_timestamp = strtotime($tmp_ende);

$tmp_date_formatted = $this->formatterTerminString($tmp_start_timestamp,$tmp_ende_timestamp,$tmp_nur_anfang,$row['termintyp'],$tmp_keine_uhrzeit);


  $my_results[$counter]['nid'] = $row['Node_ID'];
  // $my_results[$counter]['zeitraum_start'] = $tmp_start_formatted;
  // $my_results[$counter]['zeitraum_ende'] = $tmp_ende_formatted;
  $my_results[$counter]['zeitraum'] = $tmp_date_formatted;
  $my_results[$counter]['thema'] = $tmp_thema;
  $my_results[$counter]['typ'] = $tmp_typ;
  $my_results[$counter]['title'] = $tmp_title;
  $my_results[$counter]['path_alias'] = $tmp_path_alias;
  $my_results[$counter]['dozenten'] = $tmp_doz_string;


  $counter ++;
}

return $my_results;

}



/**
 * Formatter for Termin Dates
 *
 * @return string  DateString
 */

 private function formatterTerminString($utc_termin_start_timestamp,$utc_termin_ende_timestamp,$nur_anfang,$termintyp,$keine_uhrzeit) {

   // echo "<br />utc_termin_start_timestamp: " . $utc_termin_start_timestamp;
   // echo "<br />utc_termin_ende_timestamp: " . $utc_termin_ende_timestamp;


  $date_formatted = '';


  if($termintyp == 1) {
    date_default_timezone_set('UTC');
    $start_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_days');
    $ende_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_days');
  }
  else {
    date_default_timezone_set('Europe/Berlin');
    $start_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_days');
    $ende_formatted_short = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_days');
  }

  if($start_formatted_short != $ende_formatted_short) {
    $date_formatted = $start_formatted_short . ' - ' . $ende_formatted_short;
  }
  else {


    if($nur_anfang == 1) {
      $start_formatted_time = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_date_time');

      if($keine_uhrzeit == 1) {
        $date_formatted = $start_formatted_short;
      } else {
        $date_formatted = $start_formatted_short . ', ab ' . $start_formatted_time . ' Uhr';
      }
    }
    else {
      $start_formatted = \Drupal::service('date.formatter')->format($utc_termin_start_timestamp, 'uta_medium_date_days');
      $ende_formatted_time = \Drupal::service('date.formatter')->format($utc_termin_ende_timestamp, 'uta_date_time');

      // echo "<br />start_formatted: " . $start_formatted;
      // echo "<br />ende_formatted_time: " . $ende_formatted_time;


      if($keine_uhrzeit == 1) {
        $date_formatted = $start_formatted_short . ' - ' . $ende_formatted_short;
      }
      else {
        $date_formatted = $start_formatted . ' - ' . $ende_formatted_time . ' Uhr';
      }
    }


  }


  return $date_formatted;
}



}
