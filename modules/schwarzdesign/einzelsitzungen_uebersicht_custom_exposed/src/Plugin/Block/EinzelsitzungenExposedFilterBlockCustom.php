<?php

namespace Drupal\einzelsitzungen_uebersicht_custom_exposed\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;

/**
 * Provides a 'Einzelsitzungen Exposed Filter Custom' Block.
 *
 * @Block(
 *   id = "einzelsitzungen_uebersicht_custom_exposed",
 *   admin_label = @Translation("Einzelsitzungen Exposed Filter Block"),
 * )
 */
class EinzelsitzungenExposedFilterBlockCustom extends BlockBase {

  /**
   * {@inheritdoc}
   */

  public function build() {

    $build = array();
    $build['#cache']['max-age'] = 0;

    $build['#attached']['library'][] = 'einzelsitzungen_uebersicht_custom_exposed/einzelsitzungen_filter';

    $radios = "";

$einzelsitzungen_help_array = array(
  'All' => 'Alle',
  '1' => 'Körperorientiert',
  '2' => 'Gesprächsorientiert'
);

if(isset($_GET['field_einzelsitzungen_kategorie_value']) && !empty($_GET['field_einzelsitzungen_kategorie_value'])){
  $selected_val = $_GET['field_einzelsitzungen_kategorie_value'];
} else {
  $selected_val = 'All';
}

foreach($einzelsitzungen_help_array as $key => $val) {

  if($selected_val == $key) {
    $checked_flag = "checked";
  }
  else {
    $checked_flag = "";
  }
  $radios .= '
  <input type="radio" class="form-radio" id="radio-'.$key.'" name="field_einzelsitzungen_kategorie_value" value="'.$key.'" '.$checked_flag.'>
  <label for="radio-'.$key.'" class="control-label option">
  <span></span>'.$val.'
  </label>';

}

    $build['#markup'] = t('
      <form method="get" class="einzelsitzungen_kategorie">
      '.$radios.'
      </form>
      ');


    return $build;


  }

}
