<?php

/**
 * @file
 * Anbebot buchen Modul
 */

 use Drupal\Component\Render\FormattableMarkup;
 use \Drupal\node\Entity\Node;
 use Drupal\node\NodeInterface;

 function angebot_buchen_page_attachments(array &$page) {
   $page['#attached']['library'][] = 'angebot_buchen/angebot_buchen';
}


 function angebot_buchen_mail($key, &$message, $params) {
   if ($message['module'] == 'angebot_buchen') {

     $angebot_id = $params['angebot_id'];
     $node = \Drupal::entityTypeManager()->getStorage('node')->load($angebot_id);

     $angebot_titel = $node->getTitle();
     //die("Titel: ".$angebot_titel);

     $angebot_zeit_anfang = format_date(strtotime($node->get('field_zeitraum')->value), "uta_date_short", "", "Europe/Berlin");
     $angebot_zeit_ende = format_date(strtotime($node->get('field_zeitraum')->end_value), "uta_date_short", "", "Europe/Berlin");

     $angebot_uhrzeit = '';
     $angebot_uhrzeit = $node->get('field_uhrzeiten_info')->value;

     $angebot_preis = $node->get('field_preis')->value . " €";
     $angebot_anzahlung = $node->get('field_anzahlung')->value . " €";

     $angebot_dozenten = '';
     $angebot_dozent = '';

     foreach ($node->field_dozenten as $item) {
       if ($item->entity) {
         $angebot_dozent .= $item->entity->get('field_name')->value . ", ";
       }
     }

     $angebot_dozenten = substr(trim($angebot_dozent),0,-1);

     $key = $message['key'];

     $zahlungsweise_labels = [
       '0' => 'Abbuchung Gesamtbetrag',
       '1' => 'Abbuchung Anzahlung sofort und Restbetrag eine Woche vor Seminarbeginn',
       '2' => 'Abbuchung nur Anzahlung und Zahlung des Restbetrags bei Ankunft (bar oder EC)',
       '3' => 'Abbuchung nur Anzahlung und Zahlung des Restbetrags mit Ratenvertrag',
       '4' => 'Überweisung Gesamtbetrag',
       '5' => 'Überweisung Anzahlung sofort und Restbetrag eine Woche vor Seminarbeginn',
       '6' => 'Überweisung nur Anzahlung und Zahlung des Restbetrags bei Ankunft (bar oder EC)',
       '7' => 'Überweisung nur Anzahlung und Zahlung des Restbetrags mit Ratenvertrag',
     ];

     $uebernachtung_yes_no = $params['uebernachtung'] == 1 ? 'Ja' : 'Nein';

     $geschlecht_labels = [
       'frau' => 'Frau',
       'mann' => 'Herr',
     ];

     $anrede_user = $params['geschlecht'] == 'frau' ? 'Liebe' : 'Lieber';

     $arguments = [
       ':geschlecht' => $geschlecht_labels[$params['geschlecht']],
       ':anrede_user' => $anrede_user,
       ':vorname' => $params['vorname'],
       ':nachname' => $params['nachname'],
       ':firma' => $params['firma'],
       ':plz' => $params['plz'],
       ':ort' => $params['ort'],
       ':land' => $params['land'],
       ':strasse' => $params['strasse'],
       ':telefon' => $params['telefon'],
       ':uebernachtung' => $params['uebernachtung'],
       ':email' => $params['email'],
       '@sonstige_mitteilungen' => $params['sonstige_mitteilungen'],
       ':zahlungsweise' => $zahlungsweise_labels[$params['zahlungsweise']],
       ':preis_tarif' => $params['preis_tarif'],
       ':kontoinhaber' => $params['kontoinhaber'],
       ':iban' => $params['iban'],
       ':bic' => $params['bic'],
       ':name_der_bank' => $params['name_der_bank'],
       '@angebot_titel' => $angebot_titel,
       ':angebot_zeit_anfang' => $angebot_zeit_anfang,
       ':angebot_zeit_ende' => $angebot_zeit_ende,
       ':angebot_uhrzeit' => $angebot_uhrzeit,
       ':angebot_dozenten' => $angebot_dozenten,
       ':angebot_preis' => $angebot_preis,
       ':angebot_anzahlung' => $angebot_anzahlung,
       ':mandatsnr' => $params['mandatsnr'],
      ];

     $markup_einzug = 'Hiermit möchten wir dich über den Einzug von ';
     if ($params['zahlungsweise'] == 0 || $angebot_anzahlung == ' €') {
       $markup_einzug .= ':angebot_preis';
     } else {
       $markup_einzug .= ':angebot_anzahlung';
     }
     $markup_einzug .= ' zum Fälligkeitstag ' . date("d.m.Y");
     if ($params['zahlungsweise'] == 1 && $angebot_anzahlung != ' €') {
       $time_restbetrag_faellig = strtotime('-7 day' , strtotime($angebot_zeit_anfang));
       $markup_einzug .= ' und über den Einzug von ' . ($node->get('field_preis')->value - $node->get('field_anzahlung')->value). ' € zum Fälligkeitstag ' . date("d.m.Y", $time_restbetrag_faellig);
     }
     $markup_einzug .= ' informieren.';

     $markup_anzahlung = '';
     if ($params['zahlungsweise'] != 0 && $params['zahlungsweise'] != 4 && $angebot_anzahlung != ' €') {
       $markup_anzahlung = '
Anzahlung:                                     :angebot_anzahlung';
     }

     $markup_zahlungsdaten_client = '';
     $markup_zahlungsdaten_uta = '';

     if($params['zahlungsweise'] < 4) {
       $markup_zahlungsdaten_client = '
KontoinhaberIn:                              :kontoinhaber
IBAN:                                        :iban
BIC:                                         :bic
Bank:                                        :name_der_bank

Du hast dich für die Zahlung per SEPA-Lastschrift entschieden. '.$markup_einzug.' Folgende Referenzen zu deiner Information: 
Gläubiger-Identifikationsnr.: DE76ZZZ00001009535 Mandatsnummer: :mandatsnr
';
       $markup_zahlungsdaten_uta = $markup_zahlungsdaten_client;
     } else {
       $markup_zahlungsdaten_client = '
&nbsp;
<h2>Unsere Bankverbindung</h2>
UTA Cologne GmbH, Commerzbank Köln
IBAN: DE 91 3704 0044 01294 909 00
BIC: COBADEFFXXX
Bitte gib bei Überweisungen den Titel und das Datum der gebuchten Veranstaltung an.
Bei Veranstaltungen mit besonderen Teilnahmebedingungen (z.B. Vorgespräch mit der Seminarleitung) bitten wir dich, sich vor Überweisung der Anzahlung kurz mit uns in Verbindung zu setzen.
';
     }

      $message['headers']['From'] = 'Osho UTA Institut <buchung@oshouta.de>';

     switch ($key) {

   	  case 'send_mail_client':

       $message['subject'] = t('Buchungsbestätigung: @seminar', array('@seminar' => $angebot_titel));

        $text = ':anrede_user :vorname :nachname,

vielen Dank für Deine verbindliche Buchung.
&nbsp;
Du hast die folgende Veranstaltung gebucht:
&nbsp;
@angebot_titel
Termin: :angebot_zeit_anfang - :angebot_zeit_ende
:angebot_uhrzeit
DozentIn: :angebot_dozenten
Preis: :preis_tarif
&nbsp;
Deine Angaben wurden wie folgt an uns übermittelt:
&nbsp;
&nbsp;
<h2>Deine Kontaktdaten</h2>
Vorname:                                 :vorname
Nachname:                                :nachname
Firma:                                   :firma
Straße, Hausnummer:                      :strasse
Plz:                                     :plz
Ort:                                     :ort
Land:                                    :land
Telefon:                                 :telefon
Email:                                   :email
Übernachtung im Seminarraum gewünscht:   '.$uebernachtung_yes_no.'
&nbsp;
&nbsp;
<h2>Deine Zahlungsweise</h2>
Preis:                                         :preis_tarif '.$markup_anzahlung.'
Zahlungsweise:
:zahlungsweise
'.$markup_zahlungsdaten_client.'
&nbsp;
<h2>Deine Nachricht (optional)</h2>
@sonstige_mitteilungen

&nbsp;
Vor Seminarbeginn senden wir dir detaillierte Informationen zu deinem Aufenthalt bei uns. Informationen zur Anreise und Wissenswertes zum Osho UTA Institut und Umgebung findest du auf unserer Webseite.

Bei Fragen stehen wir dir gerne zur Verfügung: unter + 49 221 57407-0 oder buchung@oshouta.de

Wir freuen uns, auf deinen Besuch.

Herzliche Grüße

Die Seminarkoordination des Osho UTA Instituts

';


         $text_formatted = new FormattableMarkup($text, $arguments);
         $message['body'][] = $text_formatted->__toString();


    break;

    case 'send_mail_uta':
    $message['subject'] = t('Neue Anmeldung von @vorname @nachname', array('@vorname' => $params['vorname'], '@nachname' => $params['nachname']));

      //$token = \Drupal::token();

      $text = 'Anmeldung von: :vorname :nachname (:email)

Folgende Daten wurden übermittelt:

Angebot:
@angebot_titel
:angebot_zeit_anfang - :angebot_zeit_ende
:angebot_uhrzeit
DozentIn: :angebot_dozenten
Preis: :preis_tarif
&nbsp;
&nbsp;
<h2>Kontaktdaten</h2>
Anrede:                                      :geschlecht
Vorname:                                    :vorname
Nachname:                                 :nachname
Firma:                                         :firma
Straße, Hausnummer:                :strasse
Plz:                                             :plz
Ort:                                             :ort
Land:                                          :land
Telefon:                                      :telefon
Email:                                         :email
Übernachtung:                            '.$uebernachtung_yes_no.'

&nbsp;
&nbsp;
<h2>Zahlung</h2>
Preis:                                         :preis_tarif'.$markup_anzahlung.'
Zahlungsweise:
:zahlungsweise
'.$markup_zahlungsdaten_uta.'
&nbsp;
<h2>Nachricht</h2>
@sonstige_mitteilungen

';


      $text_formatted = new FormattableMarkup($text, $arguments);
      $message['body'][] = $text_formatted->__toString();


    break;
    }
   }
 }

?>
