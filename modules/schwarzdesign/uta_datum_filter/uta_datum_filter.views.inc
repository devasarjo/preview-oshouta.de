<?php

/**
 * Implements hook_views_data_alter().
 */
function uta_datum_filter_views_data_alter(array &$data) {

  $data['node_field_data']['nodes_datum_filter'] = array(
    'title' => t('UTA Datum Filter'),
    'filter' => array(
      'title' => t('UTA Datum Filter'),
      'help' => t('Spezieller Filter für das Datum von UTA.'),
      'field' => 'title',
      'id' => 'uta_datum_filter'
    ),
  );
}
